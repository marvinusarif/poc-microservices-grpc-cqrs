package es

import (
	"time"

	jsoniter "github.com/json-iterator/go"

	pb "bitbucket.org/evhivetech/aggregate-location-v2/server/entities/locations"
	"github.com/golang/protobuf/ptypes"
)

func CopyEvent(e Event) Event {
	return Event{
		ID:               e.ID,
		ProcessID:        e.ProcessID,
		ProcessCreatedAt: e.ProcessCreatedAt,
		SubProcessID:     e.SubProcessID,
		EventSourceID:    e.EventSourceID,
		EventSourceName:  e.EventSourceName,
		Version:          e.Version,
		Sequence:         e.Sequence,
		Type:             e.Type,
		Data:             string(e.Data),
		CreatedAt:        e.CreatedAt,
	}
}

func BuildEventsFromLocation(process *pb.Process, prev *pb.Location, curr *pb.Location) ([]Event, error) {
	var events []Event
	var data []byte

	procTS, err := ptypes.Timestamp(process.GetTimestamp())
	if err != nil {
		return events, err
	}

	_e := Event{
		ID:               0,
		ProcessID:        process.GetId(),
		ProcessCreatedAt: procTS,
		SubProcessID:     process.GetSubProcess().GetId(),
		EventSourceID:    prev.GetId(),
		EventSourceName:  ENTITY_NAME,
		Version:          prev.GetVersion(),
		Sequence:         0,
		Type:             "",
		Data:             string(data),
		CreatedAt:        time.Now(),
	}

	if prev.Version > 0 && prev.Id > 0 && process.SubProcess.Name == pb.SubProcessName_UPDATE_LOCATION {
		if prev.Name != curr.Name {
			data, _ = jsoniter.Marshal(&curr)
			_e.Type = NAME_CHANGED
			_e.Data = string(data)
			_e.Sequence++
			events = append(events, CopyEvent(_e))
		}

		if prev.Building != curr.Building {
			data, _ = jsoniter.Marshal(&curr)
			_e.Type = BUILDING_CHANGED
			_e.Data = string(data)
			_e.Sequence++
			events = append(events, CopyEvent(_e))
		}

		if prev.Description != curr.Description {
			data, _ = jsoniter.Marshal(&curr)
			_e.Type = DESCRIPTION_CHANGED
			_e.Data = string(data)
			_e.Sequence++
			events = append(events, CopyEvent(_e))
		}

		if prev.Address != curr.Address {
			data, _ = jsoniter.Marshal(&curr)
			_e.Type = ADDRESS_CHANGED
			_e.Data = string(data)
			_e.Sequence++
			events = append(events, CopyEvent(_e))
		}

		if prev.City != curr.City {
			data, _ = jsoniter.Marshal(&curr)
			_e.Type = CITY_CHANGED
			_e.Data = string(data)
			_e.Sequence++
			events = append(events, CopyEvent(_e))
		}

		if prev.State != curr.State {
			data, _ = jsoniter.Marshal(&curr)
			_e.Type = STATE_CHANGED
			_e.Data = string(data)
			_e.Sequence++
			events = append(events, CopyEvent(_e))
		}

		if prev.Country != curr.Country {
			data, _ = jsoniter.Marshal(&curr)
			_e.Type = COUNTRY_CHANGED
			_e.Data = string(data)
			_e.Sequence++
			events = append(events, CopyEvent(_e))
		}

		if prev.Zipcode != curr.Zipcode {
			data, _ = jsoniter.Marshal(&curr)
			_e.Type = ZIPCODE_CHANGED
			_e.Data = string(data)
			_e.Sequence++
			events = append(events, CopyEvent(_e))
		}

		if prev.UniqueCode != curr.UniqueCode {
			data, _ = jsoniter.Marshal(&curr)
			_e.Type = UNIQUECODE_CHANGED
			_e.Data = string(data)
			_e.Sequence++
			events = append(events, CopyEvent(_e))
		}

		if prev.Phonenumber != curr.Phonenumber {
			data, _ = jsoniter.Marshal(&curr)
			_e.Type = PHONENUMBER_CHANGED
			_e.Data = string(data)
			events = append(events, CopyEvent(_e))
		}

		if prev.NotificationEmail != curr.NotificationEmail {
			data, _ = jsoniter.Marshal(&curr)
			_e.Type = NOTIFICATIONEMAIL_CHANGED
			_e.Data = string(data)
			_e.Sequence++
			events = append(events, CopyEvent(_e))
		}

		if prev.ReplyToEmail != curr.ReplyToEmail {
			data, _ = jsoniter.Marshal(&curr)
			_e.Type = REPLYTOEMAIL_CHANGED
			_e.Data = string(data)
			_e.Sequence++
			events = append(events, CopyEvent(_e))
		}

		if prev.FromEmail != curr.FromEmail {
			data, _ = jsoniter.Marshal(&curr)
			_e.Type = FROMEMAIL_CHANGED
			_e.Data = string(data)
			_e.Sequence++
			events = append(events, CopyEvent(_e))
		}

		if prev.Timezone != curr.Timezone {
			data, _ = jsoniter.Marshal(&curr)
			_e.Type = TIMEZONE_CHANGED
			_e.Data = string(data)
			_e.Sequence++
			events = append(events, CopyEvent(_e))
		}

		if prev.Lat != curr.Lat {
			data, _ = jsoniter.Marshal(&curr)
			_e.Type = LAT_CHANGED
			_e.Data = string(data)
			_e.Sequence++
			events = append(events, CopyEvent(_e))
		}

		if prev.Lon != curr.Lon {
			data, _ = jsoniter.Marshal(&curr)
			_e.Type = LON_CHANGED
			_e.Data = string(data)
			events = append(events, CopyEvent(_e))
		}

		if prev.Slug != curr.Slug {
			data, _ = jsoniter.Marshal(&curr)
			_e.Type = SLUG_CHANGED
			_e.Data = string(data)
			_e.Sequence++
			events = append(events, CopyEvent(_e))
		}

		if prev.StatusId != curr.StatusId {
			data, _ = jsoniter.Marshal(&curr)
			_e.Type = STATUS_CHANGED
			_e.Data = string(data)
			_e.Sequence++
			events = append(events, CopyEvent(_e))
		}

	} else {
		data, _ = jsoniter.Marshal(&curr)
		_e.Type = LOCATION_ADDED
		_e.Data = string(data)
		_e.Sequence++
		events = append(events, CopyEvent(_e))
	}
	return events, err
}

func BuildLocationFromEvents(es EventSource, events []Event, ss *pb.Location) (*pb.Location, error) {
	var (
		err      error
		location *pb.Location
		temp     *pb.Location
	)

	//if snapshot exists
	if ss.Id > 0 {
		location = ss
	}

	for _, e := range events {
		jsoniter.Unmarshal([]byte(e.Data), &temp)
		switch eventType := e.Type; eventType {
		case LOCATION_ADDED:
			location = temp
		case NAME_CHANGED:
			location.Name = temp.Name
		case BUILDING_CHANGED:
			location.Building = temp.Building
		case DESCRIPTION_CHANGED:
			location.Description = temp.Description
		case ADDRESS_CHANGED:
			location.Address = temp.Address
		case CITY_CHANGED:
			location.City = temp.City
		case STATE_CHANGED:
			location.State = temp.State
		case COUNTRY_CHANGED:
			location.Country = temp.Country
		case ZIPCODE_CHANGED:
			location.Zipcode = temp.Zipcode
		case UNIQUECODE_CHANGED:
			location.UniqueCode = temp.UniqueCode
		case PHONENUMBER_CHANGED:
			location.Phonenumber = temp.Phonenumber
		case NOTIFICATIONEMAIL_CHANGED:
			location.NotificationEmail = temp.NotificationEmail
		case REPLYTOEMAIL_CHANGED:
			location.ReplyToEmail = temp.ReplyToEmail
		case FROMEMAIL_CHANGED:
			location.FromEmail = temp.FromEmail
		case TIMEZONE_CHANGED:
			location.Timezone = temp.Timezone
		case LAT_CHANGED:
			location.Lat = temp.Lat
		case LON_CHANGED:
			location.Lon = temp.Lon
		case SLUG_CHANGED:
			location.Slug = temp.Slug
		case STATUS_CHANGED:
			location.StatusId = temp.StatusId
		}
	}

	//compensate payload
	location.Id = es.ID
	location.Version = es.Version
	location.CreatedAt, _ = ptypes.TimestampProto(es.CreatedAt)
	location.UpdatedAt, _ = ptypes.TimestampProto(es.UpdatedAt)

	return location, err
}
