package es

import "time"

const ENTITY_NAME = "LOCATION"
const LOCATION_ADDED = "LOCATION_ADDED"
const NAME_CHANGED = "NAME_CHANGED"
const BUILDING_CHANGED = "BUILDING CHANGED"
const DESCRIPTION_CHANGED = "DESCRIPTION_CHANGED"
const ADDRESS_CHANGED = "ADDRESS_CHANGED"
const CITY_CHANGED = "CITY_CHANGED"
const STATE_CHANGED = "STATE_CHANGED"
const COUNTRY_CHANGED = "COUNTRY_CHANGED"
const ZIPCODE_CHANGED = "ZIPCODE_CHANGED"
const UNIQUECODE_CHANGED = "UNIQUECODE_CHANGED"
const PHONENUMBER_CHANGED = "PHONENUMBER_CHANGED"
const NOTIFICATIONEMAIL_CHANGED = "NOTIFICATIONEMAIL_CHANGED"
const REPLYTOEMAIL_CHANGED = "REPLYTOEMAIL_CHANGED"
const FROMEMAIL_CHANGED = "FROMEMAIL_CHANGED"
const TIMEZONE_CHANGED = "TIMEZONE_CHANGED"
const LAT_CHANGED = "LAT_CHANGED"
const LON_CHANGED = "LOT_CHANGED"
const SLUG_CHANGED = "SLUG_CHANGED"
const STATUS_CHANGED = "STATUS_CHANGED"

type EventSource struct {
	ID        int64
	Name      string
	Version   int64
	CreatedAt time.Time
	UpdatedAt time.Time
}

type Event struct {
	ID               int64
	ProcessID        string
	ProcessCreatedAt time.Time
	SubProcessID     string
	EventSourceID    int64
	EventSourceName  string
	Version          int64
	Sequence         int32
	Type             string
	Data             string
	CreatedAt        time.Time
}

type Snapshot struct {
	EventSourceName string
	EventSourceID   int64
	Version         int64
	Data            string
	CreatedAt       time.Time
}
