package common

import (
	"fmt"

	"github.com/BurntSushi/toml"
)

type tomlConfig struct {
	Title      string
	Server     configuration
	ETL        etl
	Eventstore eventstore
	Elastic    elastic
}

type configuration struct {
	Host     string
	GRPCPort int
	RESTPort int
}

type etl struct {
	Username string
	Password string
	Port     int
	Host     string
	DB       string
	SSLMode  string
}

type eventstore struct {
	Username string
	Password string
	Port     int
	Host     string
	DB       string
	SSLMode  string
}

type elastic struct {
	Username    string
	Password    string
	Port        int
	Host        string
	Scheme      string
	HealthCheck bool
	Sniff       bool
}

type errorMessage struct {
	Status  int    `json:"status"`
	Error   string `json:"error"`
	Message string `json:"message"`
}

type appError struct {
	Error      string `json:"error"`
	Message    string `json:"message"`
	HttpStatus int    `json:"status"`
}

type errorResource struct {
	Data errorMessage `json:"status"`
}

// AppConfig holds the configuration values from config.toml file
var AppConfig tomlConfig

// Initialize AppConfig
func initConfig() {
	loadAppConfig()
}

// Reads config.toml and decode into AppConfig
func loadAppConfig() {
	if _, err := toml.DecodeFile("config/config.toml", &AppConfig); err != nil {
		fmt.Println(err)
		return
	}
}
