package elastics

import (
	"context"
	"strconv"

	json "github.com/json-iterator/go"

	pb "bitbucket.org/evhivetech/aggregate-location-v2/server/entities/locations"
	"bitbucket.org/evhivetech/aggregate-location-v2/server/repositories"
	"github.com/labstack/gommon/log"
	"github.com/olivere/elastic"
)

type Q_DB struct {
	db *elastic.Client
}

func GetQueryClientDB(db *elastic.Client) repository.Q {
	return &Q_DB{db: db}
}

func (q *Q_DB) AddOne(location *pb.Location) (*pb.Location, error) {
	var err error

	_, err = q.db.Index().
		Index("locations").
		Type("doc").
		Id(strconv.Itoa(int(location.Id))).
		BodyJson(location).Refresh("wait_for").Do(context.Background())

	if err != nil {
		log.Error(err)
		return location, err
	}

	return location, err
}

func (q *Q_DB) FindOne(location *pb.Location) (*pb.Location, error) {
	var _location *pb.Location

	termQuery := elastic.NewTermQuery("id", strconv.Itoa(int(location.Id)))
	result, err := q.db.Search().
		Index("locations").
		Query(termQuery).
		From(0).Size(1).
		Do(context.Background())
	if err != nil {
		log.Error(err)
		return _location, err
	}

	if result.Hits.TotalHits > 0 {
		for _, hit := range result.Hits.Hits {
			err := json.Unmarshal(*hit.Source, &_location)
			if err != nil {
				log.Error(err)
				return _location, err
			}
		}
	}

	return _location, err
}

func (q *Q_DB) UpdateOne(location *pb.Location) (*pb.Location, error) {
	var err error
	_, err = q.db.Index().
		Index("locations").
		Type("doc").
		Id(strconv.Itoa(int(location.Id))).
		BodyJson(location).
		Do(context.Background())
	if err != nil {
		log.Error(err)
		return location, err
	}
	return location, err
}
