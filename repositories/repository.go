package repository

import (
	es "bitbucket.org/evhivetech/aggregate-location-v2/server/entities/events"
	pb "bitbucket.org/evhivetech/aggregate-location-v2/server/entities/locations"
)

//Using Command Query segragation
type Q interface {
	FindOne(location *pb.Location) (*pb.Location, error)
	// FindAll(params *pb.SearchLocation, limit int, offset int) ([]*pb.Location, error)
	// Count(params *pb.SearchLocation) (int, error)
	//
	AddOne(location *pb.Location) (*pb.Location, error)
	UpdateOne(location *pb.Location) (*pb.Location, error)
	// RemoveOne(location *pb.Location) (*pb.Location, error)
}

type C interface {
	WriteNewStream(proc *pb.Process, eventSource es.EventSource, eventStreams []es.Event) (*pb.Process, es.EventSource, []es.Event, error)
	AppendToStream(proc *pb.Process, eventSource es.EventSource, eventStreams []es.Event) (*pb.Process, es.EventSource, []es.Event, error)
	ReadStream(eventSource es.EventSource, minVersion int, maxVersion int) (*pb.Location, error)

	WriteSnapshot(snapshot es.Snapshot) (*pb.Location, error)
	ReadSnapshot(snapshot es.Snapshot) (*pb.Location, error)
}
