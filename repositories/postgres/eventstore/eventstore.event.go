package eventstore

import (
	"database/sql"
	"encoding/json"
	"errors"
	"time"

	log "github.com/sirupsen/logrus"

	es "bitbucket.org/evhivetech/aggregate-location-v2/server/entities/events"
	pb "bitbucket.org/evhivetech/aggregate-location-v2/server/entities/locations"
	"bitbucket.org/evhivetech/aggregate-location-v2/server/repositories"
)

type C_DB struct {
	db *sql.DB
}

func GetCommandClientDB(db *sql.DB) repository.C {
	return &C_DB{db: db}
}

func (c *C_DB) WriteNewStream(process *pb.Process, eventSource es.EventSource, eventStreams []es.Event) (*pb.Process, es.EventSource, []es.Event, error) {
	var (
		err  error
		tx   *sql.Tx
		stmt *sql.Stmt
	)

	tx, err = c.db.Begin()
	if err != nil {
		tx.Rollback()
		log.Error(err)
		return process, eventSource, eventStreams, err
	}
	//add process
	{
		procExists := 0
		if err = tx.QueryRow("SELECT COUNT(*) FROM proc WHERE id = $1", process.Id).
			Scan(&procExists); err != nil {
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		if procExists < 1 {
			stmt, err = tx.Prepare("INSERT INTO proc VALUES ($1, $2, $3, $4, $5);")
			if err != nil {
				log.Error(err)
				return process, eventSource, eventStreams, err
			}

			if _, err = stmt.Exec(
				process.Id,
				process.Name,
				process.Origin,
				eventSource.CreatedAt,
				process.UserId); err != nil {
				log.Error(err)
				tx.Rollback()
				return process, eventSource, eventStreams, err
			}
			defer stmt.Close()
		}
	}
	//record subsprocess
	{
		stmt, err = tx.Prepare("INSERT INTO sproc VALUES ($1, $2, $3, $4, $5);")
		if err != nil {
			log.Error(err)
			return process, eventSource, eventStreams, err
		}

		if _, err = stmt.Exec(
			process.SubProcess.Id,
			process.Id,
			process.SubProcess.Name,
			process.Origin,
			eventSource.CreatedAt); err != nil {
			tx.Rollback()
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		defer stmt.Close()
	}
	{
		stmt, err = tx.Prepare("INSERT INTO es VALUES (DEFAULT, $1, $2, $3, $4) RETURNING *;")
		if err != nil {
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		if err = stmt.QueryRow(eventSource.Name, eventSource.Version+1, eventSource.CreatedAt, time.Time{}).
			Scan(
				&eventSource.ID,
				&eventSource.Name,
				&eventSource.Version,
				&eventSource.CreatedAt,
				&eventSource.UpdatedAt); err != nil {
			tx.Rollback()
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		defer stmt.Close()
	}
	{
		stmt, err = tx.Prepare("INSERT INTO e VALUES (DEFAULT, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10);")
		if err != nil {
			log.Error(err)
			return process, eventSource, eventStreams, err
		}

		for _, e := range eventStreams {
			if _, err = stmt.Exec(
				process.Id,
				eventSource.CreatedAt,
				process.SubProcess.Id,
				eventSource.CreatedAt,
				eventSource.ID,
				eventSource.Name,
				eventSource.Version,
				e.Sequence,
				e.Type,
				e.Data); err != nil {
				tx.Rollback()
				log.Error(err)
				return process, eventSource, eventStreams, err
			}
		}

		defer stmt.Close()
	}

	if err = tx.Commit(); err != nil {
		log.Error(err)
		return process, eventSource, eventStreams, err
	}

	return process, eventSource, eventStreams, err
}

func (c *C_DB) AppendToStream(process *pb.Process, eventSource es.EventSource, eventStreams []es.Event) (*pb.Process, es.EventSource, []es.Event, error) {
	var (
		err         error
		tx          *sql.Tx
		stmt        *sql.Stmt
		lastVersion int64
	)

	tx, err = c.db.Begin()
	if err != nil {
		tx.Rollback()
		log.Error(err)
		return process, eventSource, eventStreams, err
	}
	//add process
	{
		procExists := 0
		if err = tx.QueryRow("SELECT COUNT(*) FROM proc WHERE id = $1", process.Id).
			Scan(&procExists); err != nil {
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		if procExists < 1 {
			stmt, err = tx.Prepare("INSERT INTO proc VALUES ($1, $2, $3, $4, $5);")
			if err != nil {
				log.Error(err)
				return process, eventSource, eventStreams, err
			}

			if _, err = stmt.Exec(
				process.Id,
				process.Name,
				process.Origin,
				eventSource.UpdatedAt,
				process.UserId); err != nil {
				log.Error(err)
				tx.Rollback()
				return process, eventSource, eventStreams, err
			}
			defer stmt.Close()
		}
	}
	//record subsprocess
	{
		stmt, err = tx.Prepare("INSERT INTO sproc VALUES ($1, $2, $3, $4, $5);")
		if err != nil {
			log.Error(err)
			return process, eventSource, eventStreams, err
		}

		if _, err = stmt.Exec(
			process.SubProcess.Id,
			process.Id,
			process.SubProcess.Name,
			process.Origin,
			eventSource.UpdatedAt); err != nil {
			tx.Rollback()
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		defer stmt.Close()
	}
	{
		if err = tx.QueryRow("SELECT ver FROM es WHERE id = $1", eventSource.ID).
			Scan(&lastVersion); err != nil {
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		if lastVersion != eventSource.Version {
			err = errors.New("concurrency exception : version does not match with command layer version")
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		stmt, err = tx.Prepare("UPDATE es SET ver = $1, ts_m = $2 WHERE id = $3 RETURNING *;")
		if err != nil {
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		if err = stmt.QueryRow(eventSource.Version+1, eventSource.UpdatedAt, eventSource.ID).
			Scan(
				&eventSource.ID,
				&eventSource.Name,
				&eventSource.Version,
				&eventSource.CreatedAt,
				&eventSource.UpdatedAt); err != nil {
			tx.Rollback()
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		defer stmt.Close()
	}
	{
		stmt, err = tx.Prepare("INSERT INTO e VALUES (DEFAULT, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10);")
		if err != nil {
			log.Error(err)
			return process, eventSource, eventStreams, err
		}

		for _, e := range eventStreams {
			if _, err = stmt.Exec(
				process.Id,
				eventSource.UpdatedAt,
				process.SubProcess.Id,
				eventSource.UpdatedAt,
				eventSource.ID,
				eventSource.Name,
				eventSource.Version,
				e.Sequence,
				e.Type,
				e.Data); err != nil {
				tx.Rollback()
				log.Error(err)
				return process, eventSource, eventStreams, err
			}
		}

		defer stmt.Close()
	}

	if err = tx.Commit(); err != nil {
		log.Error(err)
		return process, eventSource, eventStreams, err
	}

	return process, eventSource, eventStreams, err
}

func (c *C_DB) ReadStream(eventSource es.EventSource, minVersion int, maxVersion int) (*pb.Location, error) {
	var (
		err error
	)
	location := &pb.Location{}
	return location, err
}

func (c *C_DB) WriteSnapshot(snapshot es.Snapshot) (*pb.Location, error) {
	var (
		err error
	)
	location := &pb.Location{}
	return location, err
}

func (c *C_DB) ReadSnapshot(snapshot es.Snapshot) (*pb.Location, error) {
	var (
		err      error
		location *pb.Location
		data     []byte
	)

	if err = c.db.QueryRow(`SELECT data FROM ss WHERE es_id = $1
		AND es_name = $2
		AND ver < $4
		AND ts < $3`,
		snapshot.EventSourceID,
		snapshot.EventSourceName,
		snapshot.Version,
		snapshot.CreatedAt).Scan(&data); err != nil {
		log.Error(err)
		return location, err
	}

	err = json.Unmarshal(data, &location)
	if err != nil {
		log.Error(err)
		return location, err
	}

	return location, err
}
