// package etl
//
// import (
// 	"database/sql"
// 	"fmt"
//
// 	"bitbucket.org/evhivetech/aggregate-location-v2/server/common"
// 	_ "github.com/lib/pq"
// 	log "github.com/sirupsen/logrus"
// )
//
// var pgsess *sql.DB
//
// func Connect() *sql.DB {
// 	var err error
//
// 	pgsess, err = sql.Open("postgres", fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s",
// 		common.AppConfig.ETL.Host,
// 		common.AppConfig.ETL.Port,
// 		common.AppConfig.ETL.Username,
// 		common.AppConfig.ETL.Password,
// 		common.AppConfig.ETL.DB,
// 		common.AppConfig.ETL.SSLMode))
//
// 	if err != nil {
// 		log.Errorf("ETL : failed to connect to %s (PGSQL)", common.AppConfig.ETL.DB)
// 	} else {
// 		log.Infof("ETL : connected to %s (PGSQL)", common.AppConfig.ETL.DB)
// 	}
//
// 	return pgsess
// }
