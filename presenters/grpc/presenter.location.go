package grpc_presenter

import (
	"encoding/json"
	"errors"

	pb "bitbucket.org/evhivetech/aggregate-location-v2/server/entities/locations"
	"bitbucket.org/evhivetech/aggregate-location-v2/server/usecases"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type LocationGRPCPresenter struct {
	usecase usecases.UsecaseLocation
}

func NewLocationGRPCPresenter(sv *grpc.Server, uc usecases.UsecaseLocation) {
	p := &LocationGRPCPresenter{
		usecase: uc,
	}

	pb.RegisterServiceLocationServer(sv, p)
}

func (p *LocationGRPCPresenter) FindAll(ctx *pb.RequestFindAllLocations, stream pb.ServiceLocation_FindAllServer) error {
	var (
		err error
	)
	return err
}

func (p *LocationGRPCPresenter) FindOne(ctx context.Context, params *pb.RequestFindOneLocation) (*pb.ResponseFindOneLocation, error) {
	var (
		err      error
		response *pb.ResponseFindOneLocation
	)
	return response, err
}

func (p *LocationGRPCPresenter) Add(ctx context.Context, params *pb.RequestProcessLocation) (*pb.ResponseProcessLocation, error) {
	var (
		err      error
		response *pb.ResponseProcessLocation
	)

	response = &pb.ResponseProcessLocation{
		Process:  params.GetProcess(),
		Location: params.GetLocation(),
	}
	response.Process, response.Location, err = p.usecase.AddOne(params.GetProcess(), params.GetLocation())
	if err != nil {
		log.Error(err)
		response.Process.SubProcess.Error = &pb.ErrorSubProcess{
			Code:    1,
			Message: err.Error(),
		}
		ResponseError, err := json.Marshal(&response)
		if err != nil {
			log.Error(err)
			return response, err
		}
		return response, errors.New(string(ResponseError))
	}

	return response, nil
}

func (p *LocationGRPCPresenter) Update(ctx context.Context, params *pb.RequestProcessLocation) (*pb.ResponseProcessLocation, error) {
	var (
		err      error
		response *pb.ResponseProcessLocation
	)

	response = &pb.ResponseProcessLocation{
		Process:  params.GetProcess(),
		Location: params.GetLocation(),
	}

	response.Process, response.Location, err = p.usecase.UpdateOne(params.GetProcess(), params.GetLocation())

	if err != nil {
		log.Error(err)
		response.Process.SubProcess.Error = &pb.ErrorSubProcess{
			Code:    1,
			Message: err.Error(),
		}
		ResponseError, err := json.Marshal(&response)
		if err != nil {
			log.Error(err)
			return response, err
		}
		return response, errors.New(string(ResponseError))
	}

	return response, nil
}

func (p *LocationGRPCPresenter) Remove(ctx context.Context, params *pb.RequestProcessLocation) (*pb.ResponseProcessLocation, error) {
	var (
		err      error
		response *pb.ResponseProcessLocation
		proc     *pb.Process
		location *pb.Location
	)

	proc, location, err = p.usecase.RemoveOne(params.GetProcess(), params.GetLocation())

	response = &pb.ResponseProcessLocation{
		Process:  proc,
		Location: location,
	}

	return response, err
}

func (p *LocationGRPCPresenter) Rollback(ctx context.Context, params *pb.RequestRollbackLocation) (*pb.ResponseRollbackLocation, error) {
	var (
		err      error
		response *pb.ResponseRollbackLocation
	)
	return response, err
}
