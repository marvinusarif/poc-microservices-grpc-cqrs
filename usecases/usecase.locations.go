package usecases

import (
	"errors"
	"time"

	es "bitbucket.org/evhivetech/aggregate-location-v2/server/entities/events"
	pb "bitbucket.org/evhivetech/aggregate-location-v2/server/entities/locations"
	"bitbucket.org/evhivetech/aggregate-location-v2/server/repositories"
	"github.com/golang/protobuf/ptypes"
	log "github.com/sirupsen/logrus"
)

type UsecaseLocation interface {
	FindOne(params *pb.SearchLocation) (*pb.Location, error)
	FindAll(params *pb.SearchLocation, limit int, offset int) ([]*pb.Location, error)
	Count(params *pb.SearchLocation) (int, error)

	AddOne(proc *pb.Process, location *pb.Location) (*pb.Process, *pb.Location, error)
	UpdateOne(proc *pb.Process, location *pb.Location) (*pb.Process, *pb.Location, error)
	RemoveOne(proc *pb.Process, location *pb.Location) (*pb.Process, *pb.Location, error)
}

type RepositoryLocation struct {
	C repository.C
	Q repository.Q
}

func NewLocationUseCase(c repository.C, q repository.Q) UsecaseLocation {
	return &RepositoryLocation{
		C: c,
		Q: q,
	}
}

func (repo *RepositoryLocation) FindOne(params *pb.SearchLocation) (*pb.Location, error) {
	var (
		location *pb.Location
		err      error
	)
	return location, err
}

func (repo *RepositoryLocation) FindAll(params *pb.SearchLocation, limit int, offset int) ([]*pb.Location, error) {
	var (
		locations []*pb.Location
		err       error
	)
	return locations, err
}

func (repo *RepositoryLocation) Count(params *pb.SearchLocation) (int, error) {
	var (
		totalItems int
		err        error
	)
	return totalItems, err
}

func (repo *RepositoryLocation) AddOne(proc *pb.Process, location *pb.Location) (*pb.Process, *pb.Location, error) {
	var (
		err    error
		procTs time.Time
	)

	//build events from location
	procTs, err = ptypes.Timestamp(proc.GetTimestamp())
	if err != nil {
		log.Error(err)
		return proc, location, err
	}
	prev := &pb.Location{}
	eventSource := es.EventSource{
		ID:        0,
		Name:      es.ENTITY_NAME,
		Version:   0,
		CreatedAt: procTs,
		UpdatedAt: time.Time{},
	}
	eventStreams, err := es.BuildEventsFromLocation(proc, prev, location)
	if err != nil {
		log.Error(err)
		return proc, location, err
	}

	//write as new stream
	proc, eventSource, eventStreams, err = repo.C.WriteNewStream(proc, eventSource, eventStreams)
	if err != nil {
		log.Error(err)
		return proc, location, err
	}

	//no snapshot
	ss := &pb.Location{}
	location, err = es.BuildLocationFromEvents(eventSource, eventStreams, ss)
	if err != nil {
		log.Error(err)
		return proc, location, err
	}

	//create projection on query side
	location, err = repo.Q.AddOne(location)
	if err != nil {
		log.Error(err)
		return proc, location, err
	}

	return proc, location, err
}

func (repo *RepositoryLocation) UpdateOne(proc *pb.Process, location *pb.Location) (*pb.Process, *pb.Location, error) {
	var (
		err    error
		procTs time.Time
	)
	//get version from location
	prev, err := repo.Q.FindOne(location)
	if err != nil {
		log.Error(err)
		return proc, location, err
	}

	createdAt, err := ptypes.Timestamp(prev.CreatedAt)
	if err != nil {
		log.Error(err)
		return proc, location, err
	}

	procTs, err = ptypes.Timestamp(proc.GetTimestamp())
	if err != nil {
		log.Error(err)
		return proc, location, err
	}

	//check if this version does not match with current version
	if prev.Version != location.Version {
		err = errors.New("concurrency exception : version does not match with query layer version")
		log.Error(err)
		return proc, location, err
	}

	//fill in created at from previous record and updated at using process timestamp
	eventSource := es.EventSource{
		ID:        location.Id,
		Name:      es.ENTITY_NAME,
		Version:   location.Version,
		CreatedAt: createdAt,
		UpdatedAt: procTs,
	}

	eventStreams, err := es.BuildEventsFromLocation(proc, prev, location)
	if err != nil {
		log.Error(err)
		return proc, location, err
	}

	//write as new stream
	proc, eventSource, eventStreams, err = repo.C.AppendToStream(proc, eventSource, eventStreams)
	if err != nil {
		log.Error(err)
		return proc, location, err
	}

	//build location from prev ( considered as snapshot )
	location, err = es.BuildLocationFromEvents(eventSource, eventStreams, prev)
	if err != nil {
		log.Error(err)
		return proc, location, err
	}

	//create projection on query side
	location, err = repo.Q.UpdateOne(location)
	if err != nil {
		log.Error(err)
		return proc, location, err
	}

	return proc, location, err
}

func (repo *RepositoryLocation) RemoveOne(proc *pb.Process, location *pb.Location) (*pb.Process, *pb.Location, error) {
	var (
		err error
	)
	return proc, location, err
}
